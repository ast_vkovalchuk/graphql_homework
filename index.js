import Koa from 'koa';
import Apollo from 'apollo-server-koa';
import ApolloDSRest from 'apollo-datasource-rest';

const { ApolloServer, gql } = Apollo;
const { RESTDataSource } = ApolloDSRest;

const PORT = 5000;

class SpaceXAPI extends RESTDataSource {
    constructor() {
      super();
      this.baseURL = 'https://api.spacexdata.com/v3';
    }
  
    async getDragon(id) {
      return await this.get(`dragons/${id}`);
    }
  
    async getDragons() {
      return await this.get('dragons');
    }
}

const typeDefs = gql`
    type Query {
        dragon(id: ID!): Dragon
        dragons: [Dragon]
    }

    type Dragon {
        id: ID!
        name: String
        type: String
        active: Boolean
        first_flight: String
        thrusters: [DragonThruster]
        wikipedia: String
        description: String
    }

    type DragonThruster {
        type: String
        amount: Int
        pods: Int
        fuel_1: String
        fuel_2: String
    }
`;

const resolvers = {
    Query: {
        dragon: async (_, { id }, { dataSources }) => {
            return await dataSources.SpaceXAPI.getDragon(id);
        },
        dragons: async (_, _args, { dataSources }) => {
            return await dataSources.SpaceXAPI.getDragons();
        }
    }
};

const app = new Koa();
const server = new ApolloServer({
    typeDefs,
    resolvers,
    dataSources: () => {
        return {
            SpaceXAPI: new SpaceXAPI()
        };
    }
});

app
    .use(server.getMiddleware())
    .listen({ port: PORT }, async () => {
        console.log(`Server is up and ready at http://localhost:${PORT}`);
        console.log(`GraphQL path http://localhost:${PORT}${server.graphqlPath}`);
    });